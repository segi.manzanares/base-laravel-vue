<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Rule;
use App\Traits\FileAction;

class UserRequest extends FormRequest
{
    use FileAction;

    /**
     * The user files data.
     * @var array
     */
    public $fileData = [];

    /**
     * The user file types.
     * @var array
     */
    public $fileTypes = ['profile_photo'];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'nullable|min:8|confirmed',
        ];
        // Validate files data
        foreach ($this->fileTypes as $type) {
            // Validate file as array
            $rules[$type] = 'nullable|array';
            $rules[$type.".name"] = "required_with:".$type;
            $rules[$type.".contents"] = "required_with:".$type;
        }
        // If is and update request
        if (strtoupper($this->method()) === 'PUT') {
            $rules['email'] = ['nullable', 'email', Rule::unique('users')->ignore($this->route('id'))];
            // Fields are optional on update request
            if ($this->get('password') === '********') {
                $this['password'] = null;
                $this['password_confirmation'] = null;
            }
            $rules['first_name'] = 'nullable';
            $rules['last_name'] = 'nullable';
            $rules['password'] = 'nullable|min:8|confirmed';
            // Files are optional
            foreach ($this->fileTypes as $type) {
                $rules[$type] = 'nullable|array';
            }
        }
        if ($this->routeIs('users.me.update')) {
            $rules['email'] = ['nullable', 'email', Rule::unique('users')->ignore($this->user()->id)];
        }
        return $rules;
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        // Get http method
        $method = strtoupper($this->method());
        $pronoun = trans_choice('responses.pronouns.user', 1);
        if ($method === 'POST') {
            $description = trans('responses.messages.not_created', ['model' => strtolower($pronoun)]);
        }
        else {
            $description = trans('responses.messages.not_updated', ['model' => strtolower($pronoun)]);
        }
        // Delete uploaded files
        $file = null;
        foreach ($this->fileData as $file) {
            if ($file['path'] !== false) {
                unlink($file['storage_path']);
            }
        }
        throw (new ValidationException($validator))
                ->errorBag($this->errorBag)
                ->redirectTo($this->getRedirectUrl());
    }
    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $file = null;
        $this->fileData = [];
        // Base64 decode files and store them
        foreach ($this->fileTypes as $type) {
            $file = $this->get($type);
            if (!empty($file)) {
                if (isset($file['name']) && isset($file['contents'])) {
                    // Decode and store file
                    $this->fileData[$type] = [
                        'name' => $file['name'],
                        'path' => $this->uploadFile($file, 'users'),
                        'storage_path' => ''
                    ];
                    // If file was stored, attach the storage path to array file
                    if ($this->fileData[$type]['path'] !== false) {
                        $this->fileData[$type]['storage_path'] = storage_path($this->getStorageAppDir() . $this->fileData[$type]['path']);
                    }
                    // Merge input with the new file data
                    $this->merge([$type => $this->fileData[$type]]);
                }
            }
        }
        // Validate files uploaded from api
        $validator->after(function ($validator) {
            $file = null;
            foreach ($this->fileData as $i => $file) {
                // If store failed
                if ($file['path'] === false) {
                    $description = trans('validation.image', ['attribute' => trans("validation.attributes.$i")]);
                    $validator->errors()->add("$i.contents", $description);
                }
                // Validate mimetype
                else {
                    $mime = mime_content_type($file['storage_path']);
                    // Validate that file is an image
                    if (!in_array($mime, ['image/png', 'image/jpeg', 'image/gif'])) {
                        $description = trans('validation.image', ['attribute' => trans("validation.attributes.$i")]);
                        $validator->errors()->add("$i.contents", $description);
                    }
                }
            }
        });
    }
}
