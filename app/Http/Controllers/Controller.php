<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    /**
     * The number of items per page
     *
     * @var int
     */
    protected $perPage;

    public function __construct(){
        $this->perPage = 20;
    }

    /**
     * Return "pong" message, used to check connectivity and authentication.
     *
     * @return \Illuminate\Http\Response
     */
    public function ping()
    {
        return response()->json(['message' => 'pong']);
    }
}
