<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Traits\FileAction;
use App\Models\User;

class UserController extends Controller
{
    use FileAction;

    /**
     * The user file types.
     *
     * @var array
     */
    protected $fileTypes = ['profile_photo'];

    /**
     * Get a listing of users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = $this->perPage;
        $users = User::with(['profilePhoto'])->select('*', DB::raw("CONCAT_WS(' ', first_name, last_name) as full_name"));
        if (!empty($request->input('full_name'))) {
            $users->where(DB::raw("CONCAT_WS(' ', first_name, last_name)"), 'like', '%' . $request->input('full_name') . '%');
        }
        if (!empty($request->input('email'))) {
            $users->where('email', 'like', '%' . $request->input('email') . '%');
        }
        $sort = explode('|', $request->input('sort'));
        if (count($sort) === 2) {
            $users->orderBy($sort[0], $sort[1]);
        }
        else {
            $users->orderBy('full_name', 'asc');
        }
        if ($request->input('no-paginate') == 1) {
            $users = $users->get();
        }
        else {
            $perPage = $request->input('per_page', $this->perPage);
            $users = $users->paginate($perPage);
        }
        return response()->json($users);
    }

    /**
     * Show the specified resource in json format.
     *
     * @param  int  $id The user id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with(['profilePhoto'])->findOrFail($id);
        return response()->json($user);
    }

    /**
     * Store a newly created user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        // Guardar datos del usuario
        $except = array_merge($this->fileTypes, ['id', 'password_confirmation']);
        $request->merge([
            'password' => bcrypt($request->input('password')),
        ]);
        User::create($request->except($except));
        $pronoun = trans_choice('responses.pronouns.user', 1);
        $description = trans('responses.messages.created', ['model' => $pronoun]);
        return response()->json([
            'message' => $description
        ]);
    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The user id.
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $except = array_merge($this->fileTypes, ['id', 'password_confirmation']);
        if ($request->filled('password')) {
            $request->merge(['password' => bcrypt($request->password)]);
        } else {
            $except[] = 'password';
        }
        $user->update($request->except($except));
        // Enviar respuesta
        $pronoun = trans_choice('responses.pronouns.user', 1);
        $description = trans('responses.messages.updated', ['model' => $pronoun]);
        return response()->json([
            'message' => $description
        ]);
    }

    /**
     * Update the current user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateMe(UserRequest $request)
    {
        $user = $request->user();
        $data = $request->except(array_merge($this->fileTypes, [
            'id', 'password_confirmation', 'password',
        ]));
        if ($request->filled('password')) {
            $data['password'] = bcrypt($request->password);
        }
        $user->update($data);
        // Guardar archivos
        $this->storeFiles($request, $user);
        // Enviar respuesta
        $pronoun = trans_choice('responses.pronouns.profile', 1);
        $description = trans('responses.messages.updated', ['model' => $pronoun]);
        // Obtener foto de perfil
        $user->profilePhoto;
        return response()->json([
            'message' => $description,
            'data' => $user
        ]);
    }

    /**
     * Store user files.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\User $user
     *
     * @return void
     */
    private function storeFiles($request, $user)
    {
        $data = null;
        $oldFile = null;
        $file = null;
        foreach ($this->fileTypes as $type) {
            $file = $request->input($type);
            if (!empty($file)) {
                // Si el archivo es de tipo logo_photo, verificamos si existe para reemplazarlo
                $oldFile = $user->files()->where('type', $type)->first();
                $data = [
                    'name' => $file['name'],
                    'storage_path' => $file['path'],
                    'mime' => mime_content_type($file['storage_path']),
                    'size' => filesize($file['storage_path']),
                    'type' => $type
                ];
                // Si ya existe un archivo, reemplazamos
                if ($oldFile !== null) {
                    $this->deleteFile($oldFile, 'public', 'local', false, true, false);
                    $oldFile->update($data);
                } else {
                    $user->files()->create($data);
                }
            }
        }
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id The user id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $authUser = $request->user();
        $user = User::findOrFail($id);
        if ($authUser->id == $user->id) {
            return response()->json([
                'message' => Lang::get("Can't delete your own user.")
            ], 422);
        }
        $result = $user->attemptDelete();
        if ($result !== true) {
            return response()->json($result, 422);
        }
        // Modificar el email del usuario que se va a eliminar para poder reutilizarlo
        $user->email = $user->email . '|' . Carbon::now()->format('Y.m.d_H.i.s');
        //$user->save();
        $pronoun = trans_choice('responses.pronouns.user', 1);
        $description = trans('responses.messages.deleted', ['model' => $pronoun]);
        return response()->json([
            'message' => $description
        ]);
    }

    /**
     * Activate / deactivate the specified user.
     *
     * @param  int  $id The user id.
     * @return \Illuminate\Http\Response
     */
    public function activate($id, Request $request)
    {
        $user = User::findOrFail($id);
        // Actualizar estatus
        $user->update([
            'is_active' => ($user->is_active == 1) ? 0 : 1,
        ]);
        $pronoun = trans_choice('responses.pronouns.user', 1);
        $description = trans("responses.messages." . ($user->is_active == 1 ? 'activated' : 'deactivated'), ['model' => $pronoun]);
        $request->session()->flash('response', [
            'status_code' => 200,
            'message' => $description
        ]);
        return response()->json([
            'message' => $description
        ]);
    }
}
