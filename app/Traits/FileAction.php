<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;
use GuzzleHttp\Client;
use App\File;

trait FileAction {

    private function uploadFileToExternalDrive($file, $uploadDir)
    {
        $host = config('services.fileserver.url');
        $client = new Client();
        $res = $client->request('POST', $host . '/files/store', [
            'multipart' => [
                [
                    'name' => 'file',
                    'contents' => base64_decode($file['contents']),
                    'filename' => $file['name'],
                ],
            ],
            'headers' => [
                'filedir' => $uploadDir,
            ],
        ]);
        $data = json_decode($res->getBody(), true);
        return $data['url'];
    }

    private function replaceFileToExternalDrive($oldFile, array $newFile)
    {
        $host = config('services.fileserver.url');
        $client = new Client();
        $res = $client->request('PUT', $host . '/files/replace', [
            'multipart' => [
                [
                    'name' => 'file',
                    'contents' => base64_decode($newFile['contents']),
                    'filename' => $newFile['name'],
                ],
                [
                    'name' => 'oldpath',
                    'contents' => $oldFile->storage_path,
                ],
            ],
            'headers' => [
                'filedir' => $oldFile->storage_path,
            ],
        ]);
        $data = json_decode($res->getBody(), true);
        return $data['url'];
    }

    private function deleteFileToExternalDrive($filePath)
    {
        $host = config('services.fileserver.url');
        $client = new Client();
        $res = $client->request('DELETE', $host . '/files/delete', [
            'json' => [
                'filePath' => $filePath,
            ],
        ]);
        $data = json_decode($res->getBody(), true);
        return $data;
    }

    public function uploadFile(array $file, $dir = "images", $diskType = 'public')
    {
        // Quitmaos "data:image/*;base64," de la imagen para decodificar, al igual que los espacios
        $image = $file['contents'];
        if (strpos($file['contents'], "data:") !== false) {
            $image = substr($file['contents'], strpos($file['contents'], ",") + 1);
            $image = str_replace(' ', '+', $image);
        }
        $contents = base64_decode($image);
        $uniqueName = uniqid() . '-' . Str::slug(pathinfo($file['name'], PATHINFO_FILENAME));
        $extension = pathinfo($file['name'], PATHINFO_EXTENSION);
        if (!empty($extension)) {
            $uniqueName .= '.' . $extension;
        }
        $path = "$dir/".$uniqueName;
        $success = Storage::disk('public')->put($path, $contents);
        if ($success) {
            $dir = rtrim($dir, '/') . '/';
            if ($diskType == 'fileserver') {
                return $this->uploadFileToExternalDrive($file, $dir);
            }
            return $path;
        }
        return false;
    }

    public function getStoragePublicDir()
    {
        return 'storage/';
    }
    
    public function getStorageAppDir()
    {
        return 'app/public/';
    }

    public function replaceFile($oldFile, array $newFile, $storageDisk = 'public')
    {
        if ($storageDisk == 'fileserver') {
            return $this->replaceFileToExternalDrive($oldFile, $newFile);
        }
        $filePath = rtrim($oldFile->storage_path, basename($oldFile->storage_path));
        // Guardar nuevo archivo
        $contents = base64_decode($newFile['contents']);
        $uniqueName = uniqid() . '-' . Str::slug(pathinfo($newFile['name'], PATHINFO_FILENAME));
        $extension = pathinfo($newFile['name'], PATHINFO_EXTENSION);
        if (!empty($extension)) {
            $uniqueName .= '.' . $extension;
        }
        $path = "$filePath".$uniqueName;
        $success = Storage::disk('public')->put($path, $contents);
        if ($success) {
            Storage::disk($storageDisk)->delete($oldFile->storage_path);
            return $path;
        }
        return false;
    }

    public function deleteFile($file, $fileDiskType = 'public', $garbageDiskType = 'local', $moveToGarbage = false, $deleteFromDisk = false, $deleteFromDB = true)
    {
        if ($file instanceof Collection) {
            $this->deleteFileCollection($file, $fileDiskType, $garbageDiskType, $moveToGarbage, $deleteFromDisk, $deleteFromDB);
        }
        else if ($file instanceof File) {
            return $this->deleteSingleFile($file, $fileDiskType, $garbageDiskType, $moveToGarbage, $deleteFromDisk, $deleteFromDB);
        }
        return false;
    }

    private function deleteSingleFile($file, $fileDiskType, $garbageDiskType, $moveToGarbage = false, $deleteFromDisk = false, $deleteFromDB = true)
    {
        $oldLocation = $file;
        if ($file instanceof File) {
            $oldLocation = $file->storage_path;
        }
        if ($fileDiskType == 'fileserver') {
            $this->deleteFileToExternalDrive($oldLocation);
        }
        else if ($moveToGarbage) {
            $garbageDir = 'garbage';
            $exists = Storage::disk($fileDiskType)->exists($oldLocation);
            if (!$exists) {
                return false;
            }
            $newLocation = $garbageDir . '/' . $oldLocation;
            $contents = Storage::disk($fileDiskType)->get($oldLocation);
            Storage::disk($garbageDiskType)->put($newLocation, $contents);
            Storage::disk($fileDiskType)->delete($oldLocation);
            if ($file instanceof File) {
                $file->update(['storage_path' => $newLocation]);
            }
        }
        else if ($deleteFromDisk) {
            Storage::disk($fileDiskType)->delete($oldLocation);
        }
        if ($deleteFromDB) {
            if ($file instanceof File) {
                $file->delete();
            }
        }
        return true;
    }

    private function deleteFileCollection($fileCollecion, $fileDiskType, $garbageDiskType, $moveToGarbage = false, $deleteFromDisk = false, $deleteFromDB = true)
    {
        foreach ($fileCollecion as $file) {
            $this->deleteSingleFile($file, $fileDiskType, $garbageDiskType, $moveToGarbage, $deleteFromDisk, $deleteFromDB);
        }
        return true;
    }

    private function recoverFile()
    {
        // NOTE: if you wish to recover deleted files, just move files from local disk dir 'garbage' to public disk.
        // This method should work for both disk types: public and fileserver
    }
}
