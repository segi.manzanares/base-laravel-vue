<?php

namespace App\Traits;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

trait ValidateDeletion
{
    /**
     * The db driver name, ex: mysql, pgsql
     * 
     * @var String
     */
    protected $driver = null;
    
    /**
     * The database name.
     * 
     * @var String
     */
    protected $database = null;
    
    /**
     * Get the tables that are referenced by the current model.
     * 
     * @param string $referencedColumn The referenced column name.
     * @return array
     */
    public function getModelReferences($referencedColumn = null)
    {
        if (empty($referencedColumn)) {
            $referencedColumn = $this->primaryKey;
        }
        if ($this->getConnection()->getDriverName() === 'mysql') {
            $result = DB::select("" .
                    "select TABLE_NAME, COLUMN_NAME " .
                    "from INFORMATION_SCHEMA.KEY_COLUMN_USAGE " .
                    "where TABLE_SCHEMA='{$this->getConnection()->getDatabaseName()}' and " .
                        "REFERENCED_TABLE_NAME='" . $this->getTable() . "' and " .
                        "REFERENCED_COLUMN_NAME='$referencedColumn'");
        }
        else {
            $result = DB::select("" .
                    "select R.TABLE_NAME, R.COLUMN_NAME ".
                    "from INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE u ".
                    "inner join INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS FK ".
                        "on U.CONSTRAINT_CATALOG = FK.UNIQUE_CONSTRAINT_CATALOG ".
                        "and U.CONSTRAINT_SCHEMA = FK.UNIQUE_CONSTRAINT_SCHEMA ".
                        "and U.CONSTRAINT_NAME = FK.UNIQUE_CONSTRAINT_NAME ".
                    "inner join INFORMATION_SCHEMA.KEY_COLUMN_USAGE R ".
                        "ON R.CONSTRAINT_CATALOG = FK.CONSTRAINT_CATALOG ".
                        "AND R.CONSTRAINT_SCHEMA = FK.CONSTRAINT_SCHEMA ".
                        "AND R.CONSTRAINT_NAME = FK.CONSTRAINT_NAME ".
                    "WHERE U.COLUMN_NAME = '" . $referencedColumn . "' ".
                        "AND U.TABLE_SCHEMA = 'public' ".
                        "AND U.TABLE_NAME = '" . $this->getTable() . "'");
        }
        return $result;
    }
    
    /**
     * Get the active referenced tables of the current model.
     * 
     * @param string $referencedColumn The referenced column name.
     * @param array $extraTables Array of array table - field names of additional tables to look
     * for any others relationships.
     * @param array $ignoreTables Array of table names or arrays with table - name that
     * will be ignored in the searching of relationships.
     * @param array $customWheres Custom filters to be applied to the query.
     * @return boolean
     */
    public function activeReferencedTables($referencedColumn = null, array $extraTables = [], array $ignoreTables = [], array $customWheres = [])
    {
        $sizeCustomWheres = isset($customWheres) ? count($customWheres) : 0;

        if (empty($referencedColumn)) {
            $referencedColumn = $this->primaryKey;
        }
        $referencedTables = [];
        if (!empty($extraTables)) {
            foreach ($extraTables as $r) {
                $count = DB::table($r[0]);
                if (!is_array($r[1])) {
                    // Filtrar solo por el campo especificado
                    $count = $count->where($r[1], $this->getAttribute($referencedColumn));
                }
                else {
                    // Aplicar filtros a la tabla extra
                    foreach ($r[1] as $filters) {
                        $count = $count->where($filters[0], $filters[1]);
                    }
                }
                if (Schema::hasColumn($r[0], 'deleted_at')) {
                    $count->whereNull('deleted_at');
                }
                $count = $count->count();
                if ($count > 0) {
                    $referencedTables[] = $r[0];
                }
            }
        }
        // Obtener referencias al modelo
        $references = $this->getModelReferences($referencedColumn);
        // Checar si hay referencias activas al modelo
        $ignore = false;
        foreach ($references as $r) {
            $ignore = false;
            foreach ($ignoreTables as $it) {
                if (is_array($it)) {
                    if ($r->table_name === $it[0] && $r->column_name === $it[1]) {
                        $ignore = true;
                        break;
                    }
                }
                else {
                    if ($r->table_name === $it) {
                        $ignore = true;
                        break;
                    }
                }
            }
            if ($ignore) {
                continue;
            }
            $columns = DB::connection()->getSchemaBuilder()->getColumnListing($r->table_name);
            $count = DB::table($r->table_name)
                    ->where($r->column_name, $this->getAttribute($referencedColumn));
            // Si existe la columna deleted_at, checar que su valor sea nulo
            if (in_array('deleted_at', $columns)) {
                $count = $count->whereNull('deleted_at');
            }
            // Apply custom where
            /**
             * Used as 
             * [
             *   'missions' => ['expired_at is null', 'finished_at is null']
             * ]
             */
            if ($sizeCustomWheres > 0) {
                foreach ($customWheres as $kCW => $cw) {
                    if ($kCW === $r->table_name) {
                        foreach ($cw as $cq) {
                            $count->whereRaw($cq);
                        }
                    }
                }
            }
            $count = $count->count();
            if ($count > 0) {
                $referencedTables[] = $r->table_name;
            }
        }
        return $referencedTables;
    }
    
    
    /**
     * Check if the current model can be deleted, the model can be deleted only if the
     * model doesn't have active references.
     * 
     * @param string $referencedColumn The referenced column name.
     * @param array $extraTables Array of array table - field names of additional tables to look
     * for any others relationships.
     * @param array $ignoreTables Array of table names or arrays with table - name that
     * will be ignored in the searching of relationships.
     * @return boolean
     */
    public function canBeDeleted($referencedColumn = null, array $extraTables = [], array $ignoreTables = [])
    {
        $referencedTables = $this->activeReferencedTables($referencedColumn, $extraTables, $ignoreTables);
        if (count($referencedTables) > 0) {
            return false;
        }
        return true;
    }
    
    
    /**
     * Attempt to delete the current model.
     * 
     * @param string $referencedColumn The referenced column name.
     * @param array $extraTables Array of array table - field names of additional tables to look
     * for any others relationships.
     * @param array $ignoreTables Array of table names or arrays with table - name that
     * will be ignored in the searching of relationships.
     * @param array $customWheres Custom filters to be applied to the query.
     * @return boolean
     */
    public function attemptDelete($referencedColumn = null, array $extraTables = [], array $ignoreTables = [], array $customWheres = [])
    {
        $model = strtolower(str_replace('App\\', '', get_class($this)));
        $pronoun = trans_choice("responses.pronouns.$model", 1);
        // Validar si el modelo tiene referencias activas
        $referencedTables = $this->activeReferencedTables(
            $referencedColumn, $extraTables, $ignoreTables, $customWheres
        );
        $related = null;
        $description = null;
        if (count($referencedTables) > 0) {
            $related = static::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? Str::studly($referencedTables[0]) :
                    strtolower(trans_choice('responses.pronouns.' . strtolower($related), 2));
            $description = trans('responses.messages.not_deleted', [
                'model' => strtolower($pronoun),
                'related' => $related,
            ]);
            return [
                'success' => false,
                'message' => $description
            ];
        }
        $this->delete();
        return true;
    }
    
    /**
     * Get the model of the given table name.
     * 
     * @param string $table
     * @return string
     */
    public static function parseTableToModelName($table)
    {
        $modelsDictionary = [
            'User', 'File'
        ];
        $class = null;
        foreach ($modelsDictionary as $model) {
            $class = '\App\\' . $model;
            if ((new $class)->getTable() == $table) {
                return $model;
            }
        }
        return null;
    }
}
