<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class File extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['is_image', 'path'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Check if file is an image.
     *
     * @return boolean
     */
    public function getIsImageAttribute()
    {
        return Str::startsWith($this->mime, 'image/');
    }

    /**
     * Get the file full path.
     *
     * @return boolean
     */
    public function getPathAttribute()
    {
        $path = $this->storage_path;
        if (!Str::startsWith($path, "http://") && !Str::startsWith($path, "https://")) {
            $path = asset("storage/{$this->storage_path}");
        }
        return $path;
    }
}
