<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ValidateDeletion;

class User extends Authenticatable
{
    use SoftDeletes, Notifiable, ValidateDeletion;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['full_name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The related files
     */
    public function files()
    {
        return $this->morphMany('App\Models\File', 'fileable');
    }

    /**
     * Find the user's profile photo.
     *
     * @return \App\File
     */
    public function profilePhoto()
    {
        return $this->morphOne('App\Models\File', 'fileable')
                ->where('type', 'profile_photo');
    }

    /**
     * Get the user full name.
     *
     * @return string
     */
    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Get full name attribute.
     *
     * @return boolean
     */
    public function getFullNameAttribute()
    {
        return $this->fullName();
    }
}
