<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Autenticación
Route::post('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::post('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

// Recuperar contraseña
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset.store');
Route::get('password/reset/{token}', function() {
    return view('index');
})->name('password.reset');

Route::group(['middleware' => ['auth'], 'prefix' => 'ajax'], function() {
    Route::get('ping', ['as' => 'ping', 'uses' => 'Controller@ping']);
    Route::group(['namespace' => 'Admin'], function() {
        // Usuarios
        Route::get('users', ['as' => 'users.index', 'uses' => 'UserController@index']);
        Route::get('users/{id}', ['as' => 'users.show', 'uses' => 'UserController@show']);
        Route::post('users', ['as' => 'users.store', 'uses' => 'UserController@store']);
        Route::put('users/me', ['as' => 'users.me.update', 'uses' => 'UserController@updateMe']);
        Route::put('users/{id}', ['as' => 'users.update', 'uses' => 'UserController@update']);
        Route::delete('users/{id}', ['as' => 'users.destroy', 'uses' => 'UserController@destroy']);
        Route::put('users/{id}/activate', ['as' => 'users.activate', 'uses' => 'UserController@activate']);
    });
});

Route::fallback(function() {
    return view('index');
});