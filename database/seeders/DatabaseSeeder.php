<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Crear usuario default
        User::firstOrcreate(["email" => "admin@copyleft.com.mx"], [
            'first_name' => 'Admin',
            'last_name' => 'Copyleft',
            "password" => bcrypt("secret"),
        ]);
    }
}
