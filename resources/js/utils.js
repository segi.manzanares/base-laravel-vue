
/* global Swal */

function confirm(config) {
    var swalCfg = {
        title: config.title,
        text: config.msg,
        type: config.type === void 0 ? 'warning' : config.type,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "Si, hazlo",
        cancelButtonText: "Cancelar",
        buttonsStyling: false,
        customClass: {
            confirmButton: 'btn btn-primary',
            cancelButton: 'btn btn-secondary'
        }
    };
    if (config.input !== void 0) {
        swalCfg.input = config.input;
    }
    if (config.inputPlaceholder !== void 0) {
        swalCfg.inputPlaceholder = config.inputPlaceholder;
    }
    if (config.inputValidator !== void 0) {
        swalCfg.inputValidator = config.inputValidator;
    }
    Swal.fire(swalCfg).then(function(result) {
        if (result.value !== void 0) {
            if (config.input !== void 0) {
                config.callback(result.value);
            }
            else {
                config.callback();
            }
        }
    }).catch(Swal.noop);
}

function numberFormat(number, decimals, decimalPoint, thousandsSeparator) {
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousandsSeparator === 'undefined') ? ',' : thousandsSeparator,
        dec = (typeof decimalPoint === 'undefined') ? '.' : decimalPoint,
        toFixedFix = function (n, prec) {
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            var k = Math.pow(10, prec);
            return Math.round(n * k) / k;
        },
        s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

function toInteger(number) {
    var n = 0;
    n = parseInt(number);
    if (isNaN(n)) {
        n = 0;
    }
    else if (!isFinite(n)) {
        n = 0;
    }
    return n;
}

function toFloat(number) {
    var n = 0;
    n = parseFloat(number);
    if (isNaN(n)) {
        n = 0;
    }
    else if (!isFinite(n)) {
        n = 0;
    }
    return n;
}

function strToSlug(str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
  
    // Remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}

export default {
    confirmationMessage: confirm,
    numberFormat: numberFormat,
    toInteger: toInteger,
    toFloat: toFloat,
    strToSlug: strToSlug,
}