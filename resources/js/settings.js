
export default {
    vuetable: {
        css: {
            table: {
                tableWrapper: '',
                tableClass: 'table table-striped table-bordered table-hovered',
                tableHeaderClass: 'fixed',
                tableBodyClass: 'vuetable-semantic-no-top fixed',
                loadingClass: 'fa fa-spinner',
                ascendingIcon: 'fa fa-sort-up',
                descendingIcon: 'fa fa-sort-down',
                sortableIcon: 'fa fa-sort'
            },
            pagination: {
                infoClass: 'float-left',
                wrapperClass: 'vuetable-pagination float-right',
                activeClass: 'active',
                disabledClass: 'disabled',
                pageClass: 'page-item',
                linkClass: 'page-item',
                icons: {
                    first: 'fa fa-angle-double-left',
                    prev: 'fa fa-angle-left',
                    next: 'fa fa-angle-right',
                    last: 'fa fa-angle-double-right'
                }
            }
        }
    },
    toastr: {
        'closeButton': true,
        'debug': false,
        'newestOnTop': false,
        'progressBar': false,
        'positionClass': "toast-top-right",
        'preventDuplicates': true,
        'onclick': null,
        'showDuration': "300",
        'hideDuration': "1000",
        'timeOut': "10000",
        'extendedTimeOut': "1000",
        'showEasing': "swing",
        'hideEasing': "linear",
        'showMethod': "fadeIn",
        'hideMethod': "fadeOut"
    },
    session: {
        expiresIn: 180 // Tiempo de sesión en minutos
    },
    env: 'dev',
    title: "Laravel",
    modalHeight: 400
}