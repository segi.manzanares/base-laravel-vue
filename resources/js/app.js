/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

if (window.N === void 0) {
    window.N = {};
}

require('./bootstrap');
window.adminlte = require('admin-lte');
require('busy-load');
require('../../node_modules/admin-lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.js');

import Vue from 'vue';
import VueRouter from 'vue-router';
import BootstrapVue from 'bootstrap-vue';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import settings from './settings';
import utils from './utils';
import routes from './routes';
import auth from './auth';
import App from './layouts/App.vue';

window.moment = require('moment');
window.NProgress = require('nprogress');
window.toastr = require('toastr');
window.Swal = Swal;

Vue.use(VueRouter);
Vue.use(BootstrapVue);

NProgress.configure({
    showSpinner: false
});
toastr.options = settings.toastr;
// Tiempo de sesión
let lifetime = document.head.querySelector('meta[name="lifetime"]');
if (lifetime !== null) {
    settings.session.expiresIn = utils.toInteger(lifetime.content);
}
// Entorno del sistema
let env = document.head.querySelector('meta[name="env"]');
if (env !== null) {
    settings.env = env.content;
}
// Titulo del sitio
let title = document.head.querySelector('meta[name="title"]');
if (title !== null) {
    settings.title = title.content;
}

// Interceptors
axios.interceptors.request.use(
    function (config) {
        // Actualizar el timestamp del último request
        let data = JSON.parse(localStorage.getItem('session_data'));
        if (data !== null && !_.isNil(data.auth)) {
            data.auth.lastRequestAt = moment().format('YYYY-MM-DD HH:mm:ss');
            localStorage.setItem('session_data', JSON.stringify(data));
        }
        return config;
    },
    function (error) {
        // Do something with request error
        return Promise.reject(error);
    });
// Manejador de excepciones http
axios.interceptors.response.use(
    response => response,
    function (error) {
        if (error.response !== void 0) {
            // Error de autenticación
            if (error.response.status === 401 && N.vm.$router.currentRoute.name !== 'login') {
                // Redireccionar al login
                auth.logout();
            }
            else if (error.response.status === 419) {
                window.location.reload();
            }
            else if (error.response.status !== 422) {
                N.vm.$refs.app.error = {
                    status: error.response.status,
                    message: error.response.data.message
                };
            }
        }
        return Promise.reject(error);
    }
);

// Rutas del sitio
const router = new VueRouter({
    mode: 'history',
    routes: routes
});
router.beforeEach((to, from, next) => {
    if (N.vm !== void 0) {
        N.vm.$refs.app.error = null;
    }
    // Verificar si la ruta requiere autenticación
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // Si el usuario no está autenticado
        if (!auth.loggedIn()) {
            // Borrar datos de la sesión
            localStorage.setItem('session_data', null);
            // Redireccionar al login
            window.location.href = router.resolve({ name: 'login' }).href + "?redirect=" + to.fullPath;
        }
        else {
            next();
        }
    }
    else {
        next();
    }
});
router.beforeResolve((to, from, next) => {
    // Mostrar barra de progreso
    if (to.name) {
        if (to.name !== from.name) {
            if (to.name === null || from.name === null || to.name === void 0 || from.name === void 0) {
                NProgress.start();
            }
            // Si estamos intentando cargar una página desde otra del mismo módulo,
            // ejemplo: 'users' desde 'users.edit'
            else if (!from.name.startsWith(to.name.split('.')[0])) {
                NProgress.start();
            }
        }
    }
    next();
});
router.afterEach((to, from) => {
    // Ocultar barra de progreso si la ruta es alguna de las que se especifican,
    // las demás rutas ocultan la barra al finalizar la carga de las listas.
    let routes = [
        'home', 'dashboard', 'login', 'not_found', 'access_denied', 'password.reset',
        'password.request'
    ];
    if (to.name !== null && to.name !== void 0) {
        if (routes.indexOf(to.name) !== -1) {
            NProgress.done();
        }
    }
});

// Inicializar app
N.vm = new Vue({
    el: '#app',
    router,
    components: {
        'app': App
    },
    template: '<app ref="app"/>'
});
// Al finalizar la carga del sitio
$(function() {
    // Establecer la altura máxima de los modals
    settings.modalHeight = $(window).height() - 180;
    $('.modal-body').css('max-height', settings.modalHeight);
});
