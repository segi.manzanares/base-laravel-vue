
import moment from 'moment';
import utils from './utils.js';

export default {
    loggedIn: function () {
        var data = JSON.parse(localStorage.getItem('session_data'));
        if (data === null || _.isNil(data.user)) {
            return false;
        }
        if (!_.isNil(data.auth)) {
            // Verificar si ya expiró la sesión en base al último request y al tiempo de expiración
            if (!moment(data.auth.lastRequestAt, 'YYYY-MM-DD HH:mm:ss')
                    .add(data.auth.expiresIn, 'minutes')
                    .isSameOrAfter(moment())) {
                return false;
            }
        }
        return data.user !== null;
    },
    getApiToken: function () {
        var data = JSON.parse(localStorage.getItem('session_data'));
        return data !== null ? data.auth.token : null;
    },
    logout: function() {
        localStorage.setItem('session_data', null);
        axios.post('/logout')
        .then(function (response) {
            window.location.href = N.vm.$router.resolve({ name: 'login' }).href;
        })
        .catch(function (error) {
            if (error.response !== void 0) {
                if (error.response.status === 401 || error.response.status === 419) {
                    window.location.href = N.vm.$router.resolve({ name: 'login' }).href;
                }
                else {
                    toastr['error'](error.response.data.message);
                }
            }
        });
    },
    user: function() {
        var data = JSON.parse(localStorage.getItem('session_data'));
        return data !== null ? data.user : null;
    },
    isAdmin: function() {
        var data = JSON.parse(localStorage.getItem('session_data'));
        return data !== null ? (data.user ? data.user.is_admin : false) : false;
    },
    sessionExpired: function() {
        var data = JSON.parse(localStorage.getItem('session_data'));
        if (data === null) {
            return true;
        }
        var createdAt = moment(data.auth.createdAt);
        var expiresIn = utils.toInteger(data.auth.expiresIn);
        if (createdAt.add(expiresIn, 'm').isSameOrBefore(moment())) {
            return true;
        }
        return false;
    },
    hasRemember: function() {
        var data = JSON.parse(localStorage.getItem('session_data'));
        return data !== null ? data.auth.remember : false;
    }
}