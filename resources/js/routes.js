/* global auth */

import Dashboard from './views/Dashboard.vue';
import Login from './views/auth/Login.vue';
import Email from './views/auth/Email.vue';
import Reset from './views/auth/Reset.vue';
import Users from './views/users/Main.vue';
import NotFound from './views/errors/404.vue';
import AccessDenied from './views/errors/403.vue';
import InternalServerError from './views/errors/500.vue';
import auth from './auth';
import _ from 'lodash';

function Group(prefix) {
    this.routes = [];
    this.prefix = '/' + (prefix.startsWith('/') ? prefix.substr(1) : prefix);
    this.add = function(path, name, cmp, config) {
        let route = {
            path: (this.prefix === '/' ? '' : this.prefix) + (path.startsWith('/') ? path : ('/' + path.substr(1))),
            name: name,
            component: cmp
        };
        // Agregar configuración extra (meta, eventos, etc)
        if (config !== void 0) {
            for (let key in config) {
                if (config.hasOwnProperty(key)) {
                    route[key] = config[key];
                }
            }
        }
        this.routes.push(route);
    };
}
// Rutas del admin
var admin = new Group('/');
admin.add('/', 'home', Dashboard, {
    meta: {requiresAuth: true}
});
admin.add('/dashboard', 'dashboard', Dashboard, {
    meta: {requiresAuth: true}
});
admin.add('/login', 'login', Login, {
    meta: {redirectTo: '/'},
    beforeEnter: function(to, from, next) {
        if (auth.loggedIn()) {
            next({ name: 'home' });
        }
        next();
    }
});
admin.add('/404', 'not_found', NotFound);
admin.add('/403', 'access_denied', AccessDenied);
admin.add('/500', 'internal_server_error', InternalServerError);
// Usuarios
admin.add('/users', 'users', Users, {meta: {requiresAuth: true}});
admin.add('/users/:id(\\d+)/edit', 'users.edit', Users, {meta: {requiresAuth: true}});

// Restablecer contraseña
var password = new Group('password');
password.add('/reset', 'password.reset', Email, {
    beforeEnter: function(to, from, next) {
        if (auth.loggedIn()) {
            next({ name: 'home' });
        }
        next();
    }
});
password.add('/reset/:token', 'password.request', Reset, {
    beforeEnter: function(to, from, next) {
        if (auth.loggedIn()) {
            next({ name: 'home' });
        }
        next();
    }
});
// Rutas
let routes = _.concat(admin.routes, password.routes, [
    {
        path: '*',
        component: NotFound
    }
]);
export default routes;