<?php

return [
    'messages' => [
        'created' => ':model was successfully created.',
        'not_created' => 'Could not create new :model.',
        'not_updated' => 'Could not update :model.',
        'not_deleted' => 'Could not delete the :model, the :model is associated to active :related.',
        'not_found' => ':model was not found.',
        'conflict' => ':model was updated prior to your request.',
        'updated' => ':model was successfully updated.',
        'deleted' => ':model was successfully deleted.',
        'not_allowed' => 'Action over :model not allowed.',
        'confirmed' => ':model was successfully confirmed.',
        'canceled' => ':model was successfully canceled.',
        'activated' => ':model was successfully activated.',
        'deactivated' => ':model was successfully deactivated.',
        'authorized' => ':model was successfully authorized.',
        'unauthorized' => ':model was successfully unauthorized.',
        'not_permission' => "Could not execute the requested action because you don't have ':permission' permission in ':module' module.",
        'email_sent' => "The email was successfully sent to the specified recipients.",
        'already_exists' => "The :model already exists and cannot be duplicated.",
        'sorted' => "The :model was successfully sorted.",
        'invalid_request' => "Invalid request, some params are incorrect.",
        'reset_password' => "The password couldn't be updated.",
    ],

    'pronouns' => [
        'user' => 'User|Users',
    ]
];
