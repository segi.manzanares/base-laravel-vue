<?php

return [
    'messages' => [
        'created' => ':model se creó satisfactoriamente.',
        'not_created' => 'No fue posible crear :model.',
        'not_updated' => 'No fue posible actualizar :model.',
        'not_deleted' => 'No fue posible eliminar :model, :model tiene :related asociados que deben ser removidos previamente.',
        'not_found' => ':model no existe.',
        'conflict' => ':model fue actualizado antes de su petición, recargue la página y vuelva a intentarlo.',
        'updated' => ':model se actualizó satisfactoriamente.',
        'deleted' => ':model se eliminó satisfactoriamente.',
        'not_allowed' => 'No tiene permitido realizar acciones sobre :model.',
        'confirmed' => ':model se confirmó satisfactoriamente.',
        'canceled' => ':model se canceló satisfactoriamente.',
        'activated' => ':model fue habilitado satisfactoriamente.',
        'deactivated' => ':model fue deshabilitado satisfactoriamente.',
        'not_permission' => "No cuenta con los permisos suficientes para ejecutar la acción solicitada.",
        'resource_denied' => "No cuenta con los permisos suficientes para acceder el recurso solicitado.",
        'email_sent' => "El e-mail fue enviado satisfactoriamente a los destinatarios especificados.",
        'already_exists' => ":model ya existe y no puede ser duplicado.",
        'sorted' => ":model fue reordenado satisfactoriamente.",
        'invalid_request' => "Petición inválida. Algunos parámetros son incorrectos.",
        'reset_password' => "La contraseña no pudo ser actualizada.",
    ],

    'pronouns' => [
        'user' => 'El usuario|Usuarios',
        'profile' => 'El perfil|Perfiles',
        'role' => 'El rol|Roles',
    ]
];
