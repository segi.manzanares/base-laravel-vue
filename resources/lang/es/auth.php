<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Las credenciales enviadas no coinciden con los registros del sistema.',
    'throttle' => 'Demasiados intentos de login. Por favor vuelve a intentarlo en :seconds segundos.',

];
