<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contraseña debe tener al menos seis caracteres y coincidir con la confirmación.',
    'reset' => 'La contraseña ha sido restablecida.',
    'sent' => 'El link para restablecer su contraseña ha sido enviado a su dirección de correo.',
    'token' => 'El token para restablecer la contraseña es inválido.',
    'user' => "No se pudo encontrar el usuario con la dirección de correo especificada.",
    'throttled' => 'Por favor espera un momento y vuelve a intentarlo.',

];
