<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="lifetime" content="{{ config('session.lifetime') }}">
        <meta name="env" content="{{ config('app.env') }}">
        <meta name="title" content="{{ config('app.name') }}">
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
        <title>{{ config('app.name') }}</title>            
    </head>
    <body class="sidebar-mini sidebar-open layout-fixed">
        <div id="app"></div>
    </body>
    <script src="{{ mix('/js/app.js') }}"></script>
</html>