## Server requirements

* ubuntu >= 16.04 xenial
* apache2
* php >= 7.2.0
* libapache2-mod-php7.2
* bcmath PHP Extension
* openssl PHP Extension
* pdo PHP Extension
* ctype PHP Extension
* json PHP Extension
* mbstring PHP Extension
* tokenizer PHP Extension
* xml PHP Extension
* curl PHP Extension
* gd PHP Extension
* nodejs
* npm
* yarn
* composer

## Install code

**Clone project**
```
$ git@gitlab.com:segi.manzanares/base-laravel-vue.git
```
**Install dependencies**
```
$ cd admin
$ composer install
```
**Create storage link**
```
$ php artisan storage:link
```
**Install npm dependencies (on local setup)**
```
$ yarn install
```
**Compile assets (on local setup)**
```
$ npm run dev
```

## Configure environment


## Initialize db

**Run migrations**
```
$ php artisan migrate
```
**Seed db with default data**
```
$ php artisan db:seed
```
